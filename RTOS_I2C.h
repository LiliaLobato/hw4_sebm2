/*
 * RTOS_I2C.h
 *
 *  Created on: Oct 10, 2019
 *      Author: gabriel
 */

#ifndef RTOS_I2C_H_
#define RTOS_I2C_H_

#include <stdint.h>
#include "fsl_clock.h"
#include "fsl_port.h"
#include "fsl_i2c.h"

#include "FreeRTOS.h"
#include "semphr.h"


#define I2C_NUMBER_OF_CHANNELS 3
#define I2C0_SCL_PIN 2 //PTB
#define I2C0_SDA_PIN 3
#define I2C1_SCL_PIN 10 //PTC
#define I2C1_SDA_PIN 11
#define I2C2_SCL_PIN 11 //PTA
#define I2C2_SDA_PIN 12

typedef enum {I2C_0, I2C_1, I2C_2} I2C_channel_t;
typedef enum {SLAVE, MASTER} master_or_slave_t;
typedef enum {RECEIVER_MODE ,TRANSMITTER_MODE} tx_or_rx_mode_t;
typedef enum {rtos_I2C_PORT_A, rtos_I2C_PORT_B, rtos_I2C_PORT_C, rtos_I2C_PORT_D, rtos_I2C_PORT_E} rtos_I2C_PORT_t;
typedef enum {SUCCESS, FAIL} rtos_I2C_flag_t;
typedef enum {NOT_INIT, INIT } rtos_I2C_init_t;


typedef struct
{
	I2C_channel_t channel;
	uint8_t ICR_value;
	uint8_t I2C_Enable;
	uint32_t system_clock;
	uint16_t baud_rate;
	uint8_t clock_gating; //Value for I2C_n Clock Gating
	uint8_t SDA_pin; //GPIO pin set as I2C SDA
	uint8_t SCL_pin; //GPIO pin set as I2C SCL
	uint32_t I2C_Pin_config;
	rtos_I2C_PORT_t port_name;

}I2C_config_struct_t;



rtos_I2C_flag_t rtos_I2C_init(I2C_config_struct_t config);
rtos_I2C_flag_t rtos_I2C_write_byte(
		I2C_channel_t channel,
		uint8_t data,
		uint16_t data_lenght,
		i2c_direction_t direction,
		uint32_t flag,
		uint8_t slave_address,
		uint8_t subaddress,
		uint8_t subaddress_size);
rtos_I2C_flag_t rtos_I2C_read_byte(I2C_channel_t channel, uint8_t data, uint16_t lenght);
static void fsl_I2C_callback(I2C_Type *base, i2c_master_handle_t *handle, status_t status, void *userData);
static inline void enable_port_clock(rtos_I2C_PORT_t port);
static inline I2C_Type * get_I2C_channel(I2C_channel_t I2C_channel);
static inline PORT_Type * get_port_base(rtos_I2C_PORT_t port);

#endif /* RTOS_I2C_H_ */
