/*
 * RTOS_I2C.c
 *
 *  Created on: Oct 11, 2019
 *      Author: gabriel
 */
#include "RTOS_I2C.h"

typedef struct
{
	rtos_I2C_init_t is_init;
	i2c_master_handle_t fsl_I2C_handle;
	SemaphoreHandle_t readOrWrite_mutex;
	SemaphoreHandle_t readOrWrite_sem;
}rtos_I2C_hanlde_t;

rtos_I2C_hanlde_t I2C_handler[I2C_NUMBER_OF_CHANNELS] = {0};

static void fsl_I2C_callback(I2C_Type *base, i2c_master_handle_t *handle, status_t status, void *userData)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	if (kStatus_Success == status)
	{
		//Asking wich I2C channel is active, in order to give the corresponding
		if(I2C0 == base)
		{
			xSemaphoreGiveFromISR(I2C_handler[I2C_0].readOrWrite_sem, &xHigherPriorityTaskWoken );
		}
		if(I2C1 == base)
		{
			xSemaphoreGiveFromISR(I2C_handler[I2C_1].readOrWrite_sem, &xHigherPriorityTaskWoken );
		}

		if(I2C2 == base)
		{
			xSemaphoreGiveFromISR(I2C_handler[I2C_2].readOrWrite_sem, &xHigherPriorityTaskWoken );
		}
	}

	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}

rtos_I2C_flag_t rtos_I2C_init(I2C_config_struct_t config)
{
	rtos_I2C_flag_t flag = FAIL;
	i2c_master_config_t fsl_I2C_config;


	//We make sure that its a valid I2C channel
	if(config.channel < I2C_NUMBER_OF_CHANNELS)
	{
		//is the channel init already?
		if(NOT_INIT == I2C_handler[config.channel].is_init)
		{
			I2C_handler[config.channel].readOrWrite_mutex = xSemaphoreCreateMutex();
			I2C_handler[config.channel].readOrWrite_sem = xSemaphoreCreateBinary();

			enable_port_clock(config.port_name);

			I2C_MasterGetDefaultConfig(&fsl_I2C_config);
			fsl_I2C_config.enableMaster = true;
			fsl_I2C_config.baudRate_Bps = config.baud_rate;

			switch(config.channel)
			{
			case I2C_0:
				config.I2C_Pin_config = kPORT_MuxAlt2; // | kPORT_PassiveFilterEnable | kPORT_PullUp | ;
				config.SCL_pin = I2C0_SCL_PIN; //PTB2 alt 2 = I2C0_SCL
				config.SDA_pin = I2C0_SDA_PIN; //PTB3 alt 2 = I2C0_SDA
				I2C_MasterInit(get_I2C_channel(I2C_0), &fsl_I2C_config, CLOCK_GetFreq(I2C0_CLK_SRC));
				NVIC_SetPriority(I2C0_IRQn,5);

			break;

			case I2C_1:
				config.I2C_Pin_config = kPORT_MuxAlt2; // | kPORT_PassiveFilterEnable | kPORT_PullUp | ;
				config.SCL_pin = I2C1_SCL_PIN; //PTC10 alt 2 = I2C1_SCL
				config.SDA_pin = I2C1_SDA_PIN; //PTC11 alt 2 = I2C1_SDA
				I2C_MasterInit(get_I2C_channel(I2C_1), &fsl_I2C_config, CLOCK_GetFreq(I2C1_CLK_SRC));
				NVIC_SetPriority(I2C1_IRQn,5);

			break;

			case I2C_2:
				config.I2C_Pin_config = kPORT_MuxAlt5; // | kPORT_PassiveFilterEnable | kPORT_PullUp | ;
				config.SCL_pin = I2C2_SCL_PIN; //PTA11 alt 5 = I2C2_SCL
				config.SDA_pin = I2C2_SDA_PIN; //PTA12 alt 5 = I2C2_SDA
				I2C_MasterInit(get_I2C_channel(I2C_2), &fsl_I2C_config, CLOCK_GetFreq(I2C2_CLK_SRC));
				NVIC_SetPriority(I2C2_IRQn,5);

			break;
			}

			I2C_MasterTransferCreateHandle(get_I2C_channel(config.channel), &I2C_handler[config.channel].fsl_I2C_handle, fsl_I2C_callback, NULL);

			PORT_SetPinMux(get_port_base(config.port_name), config.SCL_pin, config.I2C_Pin_config);
			PORT_SetPinMux(get_port_base(config.port_name), config.SDA_pin, config.I2C_Pin_config);

			I2C_handler[config.channel].is_init = INIT;
			flag = SUCCESS;
		}
	}

	return (flag);
}

rtos_I2C_flag_t rtos_I2C_write_byte(
		I2C_channel_t channel,
		uint8_t data,
		uint16_t data_lenght,
		i2c_direction_t direction,
		uint32_t flag,
		uint8_t slave_address,
		uint8_t subaddress,
		uint8_t subaddress_size
		)
{
	rtos_I2C_flag_t flageishon = FAIL;
	i2c_master_transfer_t xfer;

	if(INIT == I2C_handler[channel].is_init) //We make sure that I2Cn channel is initialized
	{
		xfer.data = data;
		xfer.dataSize = data_lenght;
		xfer.direction = direction;
		xfer.flags = flag;
		xfer.slaveAddress = slave_address;
		xfer.subaddress = subaddress;
		xfer.subaddressSize = subaddress_size;

		xSemaphoreTake(I2C_handler[channel].readOrWrite_mutex, portMAX_DELAY); //Mutex protection

		I2C_MasterTransferNonBlocking(get_I2C_channel(channel), &I2C_handler[channel].fsl_I2C_handle, &xfer);

		xSemaphoreTake(I2C_handler[channel].readOrWrite_sem, portMAX_DELAY);
		xSemaphoreGive(I2C_handler[channel].readOrWrite_mutex);

		flageishon = SUCCESS;

	}
	return flageishon;
}

/*
rtos_I2C_flag_t rtos_I2C_read_byte(I2C_channel_t channel, uint8_t data, uint16_t lenght)
{
	i2c_master_transfer_t xfer;
	if(INIT == I2C_handler[channel].is_init) //We make sure that I2Cn channel is initialized
	{
		xfer.data = data;
		xfer.dataSize = lenght;
		xSemaphoreTake(I2C_handler[channel].readOrWrite_mutex, portMAX_DELAY); //Mutex protection

		I2C_MasterTransferNonBlocking(get_I2C_channel(channel), &I2C_handler[channel].fsl_I2C_handle, &xfer);

		xSemaphoreTake(I2C_handler[channel].readOrWrite_sem, portMAX_DELAY);
		xSemaphoreGive(I2C_handler[channel].readOrWrite_mutex);
	}
}
*/
static inline void enable_port_clock(rtos_I2C_PORT_t port)
{
	switch(port)
	{
	case rtos_I2C_PORT_A:
		CLOCK_EnableClock(kCLOCK_PortA);
		break;
	case rtos_I2C_PORT_B:
		CLOCK_EnableClock(kCLOCK_PortB);
		break;
	case rtos_I2C_PORT_C:
		CLOCK_EnableClock(kCLOCK_PortC);
		break;
	case rtos_I2C_PORT_D:
		CLOCK_EnableClock(kCLOCK_PortD);
		break;
	case rtos_I2C_PORT_E:
		CLOCK_EnableClock(kCLOCK_PortE);
		break;
	}
}

static inline I2C_Type * get_I2C_channel(I2C_channel_t I2C_channel)
{
	I2C_Type * retval = I2C0;
	switch(I2C_channel)
	{
	case I2C_0:
		retval = I2C0;
		break;

	case I2C_1:
		retval = I2C1;
		break;

	case I2C_2:
		retval = I2C2;
		break;

	}
	return retval;
}

static inline PORT_Type * get_port_base(rtos_I2C_PORT_t port)
{
	PORT_Type * port_base = PORTA;
	switch(port)
	{
	case rtos_I2C_PORT_A:
		port_base = PORTA;
		break;
	case rtos_I2C_PORT_B:
		port_base = PORTB;
		break;
	case rtos_I2C_PORT_C:
		port_base = PORTC;
		break;
	case rtos_I2C_PORT_D:
		port_base = PORTD;
		break;
	case rtos_I2C_PORT_E:
		port_base = PORTE;
		break;
	}
	return port_base;
}

